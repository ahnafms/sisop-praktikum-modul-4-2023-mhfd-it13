# sisop-praktikum-modul-4-2023-mh-it13
## Daftar Isi ##
- [anggota kelompok](#anggota-kelompok)
- [nomor 1](#nomor-1)
    - [soal  1.a](#1a)
    - [soal  1.b](#1b)
    - [soal  1.c](#1c)
    - [soal  1.d](#1d)
- [nomor 2](#nomor-2)
    - [soal  2.1](#21)
    - [soal  2.2](#22)
    - [soal  2.3](#23)
    - [soal  2.4](#24)
- [nomor 3](#nomor-3)
    - [soal  3.a](#3a)
    - [soal  3.b](#3b)
    - [hasil](#3hasil)
- [nomor 4](#nomor-4)
    - [hasil](#4hasil)
- [kendala](#kendala)


## anggota kelompok

| nrp        | nama                       |
| ---------- | -------------------------- |
| 5027211038 | Ahnaf Musyaffa             |
| 5027211051 | Wisnu Adjie Saka           |
| 5027211062 | Anisa Ghina Salsabila      |

## nomor 1
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

## 1.1 Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

`kaggle datasets download -d bryanb/fifa-player-stats-database` 

```C
const char *downloadCommand =
    "kaggle datasets download -d bryanb/fifa-player-stats-database";

int result = system(downloadCommand);
```

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut. Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

```C
system("unzip fifa-player-stats-database.zip");
```
Fungsi ini digunakan untuk unzip file player database.zip

## 1.2 kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City

```C
char *commandFormat=  "cat %s | tail -n +2 | awk -F ',' '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {print} '";
```
Kode diatas menggunakan commmand linux untuk membaca file csv kemudian menggunakan `tail -n +2` untuk membaca 2 baris dari atas. `awk -F ','` digunakan untuk memisahkan kolom dengan koma. dengan $3 adalah umur, $8 adalah nilai, dan $9 adalah club.
kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City.

## 1.3 Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

```Dockerfile
# Base Image
FROM alpine:3.14

# Set Working Directory
WORKDIR /app

COPY fifa-player-stats-database.zip .
COPY storage.c . 

# Install necessary packages
RUN apk update 
RUN apk add gcc musl-dev

RUN gcc -o storage storage.c

CMD ["./storage"]
```
Kode diatas digunakan menggunakan base image alpine, kemudian menyalin fifa-player-stats-database.zip dan storage.c ke dalam sistem. Setelah itu menginsall package seperti gcc dan menjalankan `cmd storage.c` untuk compile kode storage

## 1.4 Kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
```cmd
docker push ahnafms/storage-app:1.0.0
```
Command diatas digunakan untuk mempublish docker image ke Docker Hub

## 1.5 Kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.
```docker-compose.yaml
version: '3'

services:
  container1:
    image: ahnafms/storage-app:1.0.0
    ports: 
      - 8080:8080
  container2:
    image: ahnafms/storage-app:1.0.0
    ports: 
      - 8000:8000
  container3:
    image: ahnafms/storage-app:1.0.0
    ports: 
      - 3123:3123
  container4:
    image: ahnafms/storage-app:1.0.0
    ports: 
     - 3100:3100
  container5:
    image: ahnafms/storage-app:1.0.0
    ports: 
      - 5412:5412

```
Konfigurasi diatas membuat sebuah services yang dimana di dalam services tersebut terdapat beberapa container dengan image ahnafms/storage-app dan port yang berbeda-beda.

### 1.hasil

1. ![Screenshot_2023-06-02_at_15.56.34](/uploads/1fd5cb2978d7012f6362b3eb26c00a98/Screenshot_2023-06-02_at_15.56.34.png)
Hasil setelah menjalankan program storage

2. ![Screenshot_2023-06-02_at_16.00.11](/uploads/a3af8d1b3f85f1853e93839a401314a5/Screenshot_2023-06-02_at_16.00.11.png)
Hasil setelah menjalankan program docker compose up pada folder Barcelona dan Napoli

### Kendala 
Untuk poin e pada soal nomor 1 membingungkan, instruksinya kurang jelas pada soal.

## nomor 2

Membuat sistem manajemen folder dengan ketentuan sebagai berikut:
- Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.

Membuat satu kata yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:
- Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
- Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
- Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
- Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
- Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut. 
[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
Dengan keterangan sebagai berikut.
- STATUS: SUCCESS atau FAILED.
- dd: 2 digit tanggal.
- MM: 2 digit bulan.
- yyyy: 4 digit tahun.
- HH: 24-hour clock hour, with a leading 0 (e.g. 22).
- mm: 2 digit menit.
- ss: 2 digit detik.
- CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
- DESC: keterangan action, yaitu:
 - [User]-Create directory x.
 - [User]-Rename from x to y.
 - [User]-Remove directory x.
 - [User]-Remove file x.



### 2.1
```C
#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

// Path folder log
const char *LOG_PATH = "/path/to/logmucatatsini.txt";

// Fungsi untuk mencatat kegiatan ke dalam file logmucatatsini.txt
void logActivity(const char *command, const char *description, int status)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    char timestamp[20];
    sprintf(timestamp, "%02d/%02d/%04d-%02d:%02d:%02d",
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900,
            tm.tm_hour, tm.tm_min, tm.tm_sec);

    char statusStr[8];
    if (status == 0)
    {
        strcpy(statusStr, "SUCCESS");
    }
    else
    {
        strcpy(statusStr, "FAILED");
    }

    FILE *logFile = fopen(LOG_PATH, "a");
    if (logFile == NULL)
    {
        printf("Failed to open log file.\n");
        return;
    }

    fprintf(logFile, "%s::%s::%s::%s\n", statusStr, timestamp, command, description);
    fclose(logFile);
}

// Implementasikan fungsi-fungsi FUSE berikut ini:

// germa_getattr : Fungsi ini digunakan untuk mendapatkan atribut dari sebuah file atau direktori
static int germa_getattr(const char *path, struct stat *stbuf)
{
    int res = lstat(path, stbuf);
    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

// germa_readdir: Fungsi ini digunakan untuk membaca isi dari sebuah direktori. 
static int germa_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL)
    {
        return -errno;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = entry->d_ino;
        st.st_mode = entry->d_type << 12;

        if (filler(buf, entry->d_name, &st, 0))
        {
            break;
        }
    }

    closedir(dir);
    return 0;
}

// germa_mkdir: Fungsi ini digunakan untuk membuat direktori baru
static int germa_mkdir(const char *path, mode_t mode)
{
    int res = mkdir(path, mode);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Create directory %s", path);
    logActivity("MKDIR", description, 0);

    return 0;
}

// germa_rename: Fungsi ini digunakan untuk mengubah nama sebuah file atau direktori
static int germa_rename(const char *oldpath, const char *newpath)
{
    int res = rename(oldpath, newpath);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Rename from %s to %s", oldpath, newpath);
    logActivity("RENAME", description, 0);

    return 0;
}

// germa_unlink: Fungsi ini digunakan untuk menghapus sebuah file
static int germa_unlink(const char *path)
{
    int res = unlink(path);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Remove file %s", path);
    logActivity("RMFILE", description, 0);

    return 0;
}

// germa_rmdir: Fungsi ini digunakan untuk menghapus sebuah direktori. 
static int germa_rmdir(const char *path)
{
    int res = rmdir(path);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Remove directory %s", path);
    logActivity("RMDIR", description, 0);

    return 0;
}

// germa_oper : objek struct fuse_operations yang berisi fungsi-fungsi yang akan digunakan oleh FUSE
static struct fuse_operations germa_oper = {
    .getattr = germa_getattr,
    .readdir = germa_readdir,
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .unlink = germa_unlink,
    .rmdir = germa_rmdir,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &germa_oper, NULL);
}
  
```

### 2.hasil
Untuk hasil ketika codingannya di compile, codingannya error dan tidak bisa jalan: 
![Screenshot_2023-06-03_182112](/uploads/037befb44cf695c6adde4a53f31c9179/Screenshot_2023-06-03_182112.png)

## Kendala
Codingannya error, dan tidak bisa jalan


## nomor 3
Pada nomor ketiga ini kita diminta untuk membantu Dhafin melakukan berbagai hal sebagai berikut : 

 - Pertama kita harus membuat sebuah FUSE yang termodifikasi dengan source mount nya adalah file yang telah disediakan oleh pembuat soal dan memberikan program nya dengan nama "secretadmirer.c" 

 - Yang kedua kita harus membuat agar semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64. Encode berlaku rekursif.

 - Yang ketiga kita diminta untuk membuat  semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter. 

 - Yang terakhir kita diminta agar setiap ingin membuka file, maka harus memasukkan password terlebih dahulu.

Dan berikut adalah program dari empat poin diatas : 

```c
char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length)
{
    BIO *bio, *b64;
    FILE *stream;
    size_t encoded_length;

    *output_length = 4 * ((input_length + 2) / 3); // Panjang encoded dalam Base64

    char *encoded_data = (char *)malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    // Inisialisasi BIO untuk melakukan enkripsi Base64
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); // Set flag agar hasil enkripsi tidak mengandung newline

    BIO_write(bio, data, input_length);
    BIO_flush(bio);

    // Baca hasil enkripsi dari BIO
    encoded_length = BIO_read(bio, encoded_data, *output_length);
    if (encoded_length < 0)
    {
        BIO_free_all(bio);
        free(encoded_data);
        return NULL;
    }

    BIO_free_all(bio);

    return encoded_data;
}
```
Fungsi `base64_encode` digunakan untuk mengenkripsi data menggunakan algoritma Base64. Pertama, panjang hasil enkripsi dalam format Base64 dihitung berdasarkan panjang data input. Memori yang cukup dialokasikan untuk menyimpan hasil enkripsi. Selanjutnya, inisialisasi BIO (Basic Input/Output) dilakukan untuk melakukan enkripsi Base64. Flag `BIO_FLAGS_BASE64_NO_NL` diatur agar hasil enkripsi tidak mengandung newline. Data input ditulis ke dalam BIO dan kemudian di-flush. Hasil enkripsi dibaca dari BIO dan disimpan dalam memori yang telah dialokasikan sebelumnya. Jika terjadi kesalahan saat membaca hasil enkripsi, semua objek BIO dibebaskan dan memori yang telah dialokasikan juga di-free. Akhirnya, hasil enkripsi dikembalikan sebagai output.


```c
void encryptNameIfShort(char *name)
{
    size_t nameLength = strlen(name);
    if (nameLength <= 4)
    {
        char encodedName[10];
        size_t encodedLength;
        for (int i = 0; i < nameLength; i++)
        {
            sprintf(encodedName + i * 2, "%02X", name[i]);
        }
        encodedName[nameLength * 2] = '\0';

        memcpy(name, encodedName, strlen(encodedName));
    }
}
```

Fungsi `encryptNameIfShort` digunakan untuk mengenkripsi nama file atau direktori jika panjang namanya kurang dari atau sama dengan 4 karakter. Pertama, panjang nama dihitung menggunakan fungsi `strlen`. Jika panjang nama kurang dari atau sama dengan 4, maka dilakukan proses enkripsi. Sebuah array `encodedName` dengan panjang 10 karakter dialokasikan untuk menyimpan nama yang telah dienkripsi. Selanjutnya, sebuah loop dilakukan untuk mengkonversi setiap karakter dari nama menjadi representasi heksadesimal dalam format string, lalu hasilnya ditambahkan ke `encodedName`. Setelah loop selesai, karakter null-terminating ditambahkan ke `encodedName`. Terakhir, hasil enkripsi disalin kembali ke variabel `name` menggunakan fungsi `memcpy`.

```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    // Mengenkripsi direktori dan file yang diawali dengan huruf L, U, T, dan H
    if (path[1] == 'L' || path[1] == 'U' || path[1] == 'T' || path[1] == 'H')
    {
        // Konversi stbuf->st_ino menjadi string
        char ino_str[20];
        sprintf(ino_str, "%lu", stbuf->st_ino);

        // Enkripsi dengan Base64
        size_t encoded_length;
        char *encoded_ino = base64_encode((const unsigned char *)ino_str, strlen(ino_str), &encoded_length);

        // Konversi hasil enkripsi kembali menjadi ino_t
        stbuf->st_ino = (ino_t)strtoul(encoded_ino, NULL, 10);

        free(encoded_ino);
    }

    return 0;
}
```
Fungsi xmp_getattr digunakan untuk mendapatkan atribut (metadata) dari suatu file atau direktori yang diidentifikasi oleh path. Pertama, variabel fpath dibentuk dengan menggabungkan dirpath dan path menggunakan sprintf, yang akan menjadi jalur lengkap ke file atau direktori tersebut. Selanjutnya, fungsi lstat dipanggil dengan menggunakan fpath untuk mengambil atribut file/direktori dan menyimpannya di dalam struktur stbuf. Jika panggilan lstat berhasil, maka nilai res akan menjadi 0. Namun, jika terjadi kesalahan, fungsi akan mengembalikan nilai negatif yang menunjukkan kode kesalahan menggunakan errno.

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        // Mengenkripsi direktori dan file yang diawali dengan huruf L, U, T, dan H
        if (de->d_name[0] == 'L' || de->d_name[0] == 'U' || de->d_name[0] == 'T' || de->d_name[0] == 'H')
        {
            // Konversi st.st_ino menjadi string
            char ino_str[20];
            sprintf(ino_str, "%lu", st.st_ino);

            // Enkripsi dengan Base64
            size_t encoded_length;
            char *encoded_ino = base64_encode((const unsigned char *)ino_str, strlen(ino_str), &encoded_length);

            // Konversi hasil enkripsi kembali menjadi ino_t
            st.st_ino = (ino_t)strtoul(encoded_ino, NULL, 10);

            free(encoded_ino);
        }

        // Mengubah nama file/direktori jika panjang namanya <= 4
        encryptNameIfShort(de->d_name);

        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}
```
Fungsi xmp_readdir digunakan untuk membaca isi dari suatu direktori yang diidentifikasi oleh path. Pertama, variabel fpath dibentuk dengan menggabungkan dirpath dan path menggunakan sprintf, yang akan menjadi jalur lengkap ke direktori tersebut. Kemudian, dilakukan pembukaan direktori menggunakan opendir dengan fpath sebagai parameter. Jika pembukaan direktori berhasil, dp akan berisi pointer ke struktur DIR.

Selanjutnya, dilakukan perulangan menggunakan readdir untuk membaca setiap entri dalam direktori. Setiap entri yang dibaca disimpan dalam struktur dirent dengan menggunakan variabel de. Untuk setiap entri, struktur stat (st) dibuat dan diatur nilainya dengan menggunakan informasi yang ada pada de, yaitu nomor inode (d_ino) dan tipe (d_type). Jika entri dimulai dengan huruf 'L', 'U', 'T', atau 'H', maka nomor inode dienkripsi. Langkah enkripsi ini sama dengan yang dilakukan pada fungsi xmp_getattr.

```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    // Meminta pengguna memasukkan password hanya jika nama direktori adalah "DIR_NAME"
    if (strcmp(path, encryptedDirName) == 0) {
        // Meminta pengguna memasukkan password
        char userPassword[100];
        printf("Password: ");
        fgets(userPassword, sizeof(userPassword), stdin);

        // Membandingkan password yang dimasukkan dengan password yang diharapkan
        if (strcmp(userPassword, password) != 0) {
            // Jika password salah, menolak akses
            res = -EACCES;
            close(fd);
            return res;
        }
    }

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}
```
Fungsi xmp_read digunakan untuk membaca isi dari file yang diidentifikasi oleh path. Pertama, variabel fpath dibentuk dengan menggabungkan dirpath dan path menggunakan sprintf, yang akan menjadi jalur lengkap ke file tersebut.

Selanjutnya, dilakukan pembukaan file menggunakan open dengan fpath sebagai parameter. Jika pembukaan file gagal, fungsi mengembalikan nilai -errno yang menunjukkan kesalahan yang terjadi.


```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
};
```
Struktur xmp_oper merupakan variabel bertipe struct fuse_operations yang digunakan untuk mengatur operasi- operasi yang dilakukan pada file system melalui FUSE (Filesystem in Userspace).


## kendala
1. cukup lama stuck dibagian cron karena tidak jalan-jalan tapi akhirnya bisa jalan juga

