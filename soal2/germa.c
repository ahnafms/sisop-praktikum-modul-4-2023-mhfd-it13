#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

// Path folder log
const char *LOG_PATH = "/path/to/logmucatatsini.txt";

// Fungsi untuk mencatat kegiatan ke dalam file logmucatatsini.txt
void logActivity(const char *command, const char *description, int status)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    char timestamp[20];
    sprintf(timestamp, "%02d/%02d/%04d-%02d:%02d:%02d",
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900,
            tm.tm_hour, tm.tm_min, tm.tm_sec);

    char statusStr[8];
    if (status == 0)
    {
        strcpy(statusStr, "SUCCESS");
    }
    else
    {
        strcpy(statusStr, "FAILED");
    }

    FILE *logFile = fopen(LOG_PATH, "a");
    if (logFile == NULL)
    {
        printf("Failed to open log file.\n");
        return;
    }

    fprintf(logFile, "%s::%s::%s::%s\n", statusStr, timestamp, command, description);
    fclose(logFile);
}

// Implementasikan fungsi-fungsi FUSE berikut ini:

static int germa_getattr(const char *path, struct stat *stbuf)
{
    int res = lstat(path, stbuf);
    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

static int germa_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL)
    {
        return -errno;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = entry->d_ino;
        st.st_mode = entry->d_type << 12;

        if (filler(buf, entry->d_name, &st, 0))
        {
            break;
        }
    }

    closedir(dir);
    return 0;
}

static int germa_mkdir(const char *path, mode_t mode)
{
    int res = mkdir(path, mode);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Create directory %s", path);
    logActivity("MKDIR", description, 0);

    return 0;
}

static int germa_rename(const char *oldpath, const char *newpath)
{
    int res = rename(oldpath, newpath);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Rename from %s to %s", oldpath, newpath);
    logActivity("RENAME", description, 0);

    return 0;
}

static int germa_unlink(const char *path)
{
    int res = unlink(path);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Remove file %s", path);
    logActivity("RMFILE", description, 0);

    return 0;
}

static int germa_rmdir(const char *path)
{
    int res = rmdir(path);
    if (res == -1)
    {
        return -errno;
    }

    char description[256];
    snprintf(description, sizeof(description), "[User]-Remove directory %s", path);
    logActivity("RMDIR", description, 0);

    return 0;
}

static struct fuse_operations germa_oper = {
    .getattr = germa_getattr,
    .readdir = germa_readdir,
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .unlink = germa_unlink,
    .rmdir = germa_rmdir,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &germa_oper, NULL);
}
