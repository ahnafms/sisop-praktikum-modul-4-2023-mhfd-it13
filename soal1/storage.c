#include <stdio.h>
#include <stdlib.h>

#define MAX_BUFFER_SIZE 1024

void read_csv() {
  const char *csvFilePath = "FIFA23_official_data.csv";
  char *commandFormat=  "cat %s | tail -n +2 | awk -F ',' '$3 > 25 && $8 > 85 && $9 != \"Manchester City\" {print} '";
  char command[MAX_BUFFER_SIZE];
  snprintf(command, sizeof(command), commandFormat, csvFilePath);

  FILE *fileStream = popen(command, "r");
  if (fileStream == NULL) {
    printf("Failed to open the command.\n");
  }

  char line[MAX_BUFFER_SIZE];
  while (fgets(line, sizeof(line), fileStream)) {
    printf("%s\n", line);
  }

  pclose(fileStream);
}

int main() {
  const char *downloadCommand =
      "kaggle datasets download -d bryanb/fifa-player-stats-database";

  int result = system(downloadCommand);
  if (result == 0) {
    printf("unsucessfuly install kaggle");
  } else if (result == 1) {
    printf("download kaggle succesful");
  }
  system("unzip fifa-player-stats-database.zip");
  read_csv();
  return 0;
}
