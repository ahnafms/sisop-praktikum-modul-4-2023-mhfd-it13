#define FUSE_USE_VERSION 28
#define _XOPEN_SOURCE 700

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>
#include <stdlib.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

static const char *dirpath = "/home/kali/inifolderetc/sisop";
static const char *password = "SISOP\n";
static const char *encryptedDirName = "DIR_NAME";

// Fungsi untuk melakukan enkripsi Base64
char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length)
{
    BIO *bio, *b64;
    FILE *stream;
    size_t encoded_length;

    *output_length = 4 * ((input_length + 2) / 3); // Panjang encoded dalam Base64

    char *encoded_data = (char *)malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    // Inisialisasi BIO untuk melakukan enkripsi Base64
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); // Set flag agar hasil enkripsi tidak mengandung newline

    BIO_write(bio, data, input_length);
    BIO_flush(bio);

    // Baca hasil enkripsi dari BIO
    encoded_length = BIO_read(bio, encoded_data, *output_length);
    if (encoded_length < 0)
    {
        BIO_free_all(bio);
        free(encoded_data);
        return NULL;
    }

    BIO_free_all(bio);

    return encoded_data;
}

// Fungsi untuk mengubah nama file/direktori jika panjang namanya <= 4
void encryptNameIfShort(char *name)
{
    size_t nameLength = strlen(name);
    if (nameLength <= 4)
    {
        char encodedName[10];
        size_t encodedLength;
        for (int i = 0; i < nameLength; i++)
        {
            sprintf(encodedName + i * 2, "%02X", name[i]);
        }
        encodedName[nameLength * 2] = '\0';

        memcpy(name, encodedName, strlen(encodedName));
    }
}

// Fungsi untuk melakukan enkripsi Base64 rekursif pada direktori dan file
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    // Mengenkripsi direktori dan file yang diawali dengan huruf L, U, T, dan H
    if (path[1] == 'L' || path[1] == 'U' || path[1] == 'T' || path[1] == 'H')
    {
        // Konversi stbuf->st_ino menjadi string
        char ino_str[20];
        sprintf(ino_str, "%lu", stbuf->st_ino);

        // Enkripsi dengan Base64
        size_t encoded_length;
        char *encoded_ino = base64_encode((const unsigned char *)ino_str, strlen(ino_str), &encoded_length);

        // Konversi hasil enkripsi kembali menjadi ino_t
        stbuf->st_ino = (ino_t)strtoul(encoded_ino, NULL, 10);

        free(encoded_ino);
    }

    return 0;
}

// Fungsi untuk melakukan enkripsi Base64 rekursif pada direktori dan file
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        // Mengenkripsi direktori dan file yang diawali dengan huruf L, U, T, dan H
        if (de->d_name[0] == 'L' || de->d_name[0] == 'U' || de->d_name[0] == 'T' || de->d_name[0] == 'H')
        {
            // Konversi st.st_ino menjadi string
            char ino_str[20];
            sprintf(ino_str, "%lu", st.st_ino);

            // Enkripsi dengan Base64
            size_t encoded_length;
            char *encoded_ino = base64_encode((const unsigned char *)ino_str, strlen(ino_str), &encoded_length);

            // Konversi hasil enkripsi kembali menjadi ino_t
            st.st_ino = (ino_t)strtoul(encoded_ino, NULL, 10);

            free(encoded_ino);
        }

        // Mengubah nama file/direktori jika panjang namanya <= 4
        encryptNameIfShort(de->d_name);

        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    // Meminta pengguna memasukkan password hanya jika nama direktori adalah "DIR_NAME"
    if (strcmp(path, encryptedDirName) == 0) {
        // Meminta pengguna memasukkan password
        char userPassword[100];
        printf("Password: ");
        fgets(userPassword, sizeof(userPassword), stdin);

        // Membandingkan password yang dimasukkan dengan password yang diharapkan
        if (strcmp(userPassword, password) != 0) {
            // Jika password salah, menolak akses
            res = -EACCES;
            close(fd);
            return res;
        }
    }

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
